make re
echo ==Test 00== "\"3 - 2\""
./eval_expr "3 - 2"
echo

echo ==Test 01== "\"1 + 2 * 4 % 2\""
./eval_expr "1 + 2 * 4 % 2"
echo

echo ==Test 02== "\"3 + 42 * (1 - 2 / (3 + 4) - 1 % 21) + 2\""
./eval_expr "3 + 42 * (1 - 2 / (3 + 4) - 1 % 21) + 2"
echo

echo ==Test 03== "\"42\""
./eval_expr "42"
echo

echo ==Test 04== "\"-42\""
./eval_expr "-42"
echo

echo ==Test 05== "\"1+12\""
./eval_expr "1+12"
echo

echo ==Test 06== "\"3+42*(1-2/(3+4)-1%21)+1\""
./eval_expr "3+42*(1-2/(3+4)-1%21)+1"
echo

echo ==Test 07== "\"1 +2 *( - 1-1 *( 22+2*1)+1* ( - 1 + 1* 1))\""
./eval_expr "1 +2 *( - 1-1 *( 22+2*1)+1* ( - 1 + 1* 1))"
echo

echo ==Test 08== "\"((((((((((0))))))))))\""
./eval_expr "((((((((((0))))))))))"
echo

echo ==Custom== "\"3 + 42 * ( 1 - 2 / ( 3 + 4 ) - 1 % 21 ) + 1\""
./eval_expr "3 + 42 * ( 1 - 2 / ( 3 + 4 ) - 1 % 21 ) + 1"
make fclean